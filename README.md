# Pengcitraan

## Konten Berkas
- `Pengcit-Code-DarkChannelPrior-Pengcitraan.ipynb` berisi metode konvensional
- `Pengcit-Code-MPDRNetTesting-Pengcitraan.ipynb` berisi tahap pengujian untuk metode deep learning
- `Pengcit-Code-MPDRNetTraining-Pengcitraan.ipynb` berisi tahap pelatihan untuk metode deep learning
- `Pengcit-Report-Pengcitraan.pdf` berisi laporan
- `Pengcit-Report-Pengcitraan.tex` berisi laporan berformat `.tex` untuk lengkapnya dapat dilihat di https://www.overleaf.com/read/dsnfrqgrfhvd
- `README.md` berisi penjelasan konten berkas
- Result: 
    - Avg DCP = 0.29
    - Avg MPDR = 0.85
